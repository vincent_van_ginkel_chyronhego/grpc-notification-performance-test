﻿using System;
using System.Threading.Tasks;
using Grpc.Net.Client;
using pbc = global::Google.Protobuf.Collections;

using GrpcDemo.Server;
using System.Threading;
using Grpc.Core;

namespace GrpcDemo.Client
{
    public class ChuckNorrisJokesClient
    {
        private ChuckNorrisJokes.ChuckNorrisJokesClient Client;
        private Boolean Closed = false;

        public ChuckNorrisJokesClient(string address)
        {
            var channel = GrpcChannel.ForAddress(address);
            Client = new ChuckNorrisJokes.ChuckNorrisJokesClient(channel);
        }

        public async void SubscribeToJokes(Action<string> callback, int maxMessages)
        {
            var emptyRequest = new EmptyMessage();
            var metadata = new Metadata();
            metadata.Add( "id", "TestID" );

            var responseStream = Client.SubscribeToJokes(emptyRequest, metadata);

            var receivedMessages = 0;

            while (!Closed)
            {
                if (receivedMessages >= maxMessages)
                {
                    responseStream.ResponseStream.MoveNext(new CancellationToken(true));
                    break;
                }
                else {
                    await responseStream.ResponseStream.MoveNext(CancellationToken.None);

                    var feature = responseStream.ResponseStream.Current;
                    callback("Received " + feature.ToString());

                    receivedMessages++;
                }
            }
        }

        public void Close()
        {
            Closed = true;
            Client = null;
        }

        public async Task<pbc.RepeatedField<ChuckNorrisJoke>> GetJokes()
        {
            var emptyRequest = new EmptyMessage();
            var reply = await Client.ListJokesAsync(emptyRequest);

            return reply.Jokes;
        }

        public async Task AddJoke(NewChuckNorrisJoke joke)
        {
            await Client.AddJokeAsync(joke);
        }
    }
}