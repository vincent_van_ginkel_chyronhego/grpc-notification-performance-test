using Grpc.Net.Client;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using GrpcDemo.Client;
using System;
using System.Threading.Tasks;

namespace GrpcDemo.Server
{
    public class Program
    {
        private static ChuckNorrisJokesClient Client;

        private static int ReceivedJokes = 0;
        private static int NumberOfSubscriptions = 100;

        public static void Main(string[] args)
        {
            SetupClient();

            for (int i = 0; i < NumberOfSubscriptions; i++)
            {
                SetupSubscription();
            }

            CreateServer(args);

            Console.ReadLine();
        }

        public static void CreateServer(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                }).Build().RunAsync();

        private static void SetupClient()
        {
            Client = new ChuckNorrisJokesClient("https://localhost:5001");
        }

        public static void SetupSubscription()
        {
            Client.SubscribeToJokes(LogSubscriptionResponse, int.MaxValue);
        }

        private static void LogSubscriptionResponse(string joke)
        {
            Console.Write("\r{0} Received Jokes on {1} subscribers", ++ReceivedJokes, NumberOfSubscriptions);
        }
    }
}
