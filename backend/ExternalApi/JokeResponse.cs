using System.Collections.Generic;

namespace GrpcDemo.Server.ExternalApi
{
    record JokeResponse
    {
        public string id { get; set; }
        public string url { get; set; }
        public string icon_url { get; set; }
        public string value { get; set; }
        public List<string> categoriers { get; set; }
        public string created_at { get; set; }
    }
}