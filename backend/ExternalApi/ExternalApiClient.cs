using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace GrpcDemo.Server.ExternalApi
{
    public class ExternalApiClient
    {
        public async Task<NewChuckNorrisJoke> GetRandomJoke()
        {
            using var client = new HttpClient();

            return new NewChuckNorrisJoke
            {
                Id = "ID",
                Url = "URL",
                IconUrl = "URL",
                Value = "VALUE",
            };
        }
    }
}