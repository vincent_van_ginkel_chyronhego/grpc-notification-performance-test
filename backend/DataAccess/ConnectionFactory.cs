using Microsoft.Data.Sqlite;

namespace GrpcDemo.Server.DataAccess
{
    public class ConnectionFactory
    {
        private readonly string connectionString;

        public ConnectionFactory(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public SqliteConnection createConnection()
        {
            return new SqliteConnection(connectionString);
        }
    }
}