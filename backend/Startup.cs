﻿using GrpcDemo.Server.Api;
using GrpcDemo.Server.DataAccess;
using GrpcDemo.Server.ExternalApi;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace GrpcDemo.Server
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; set; }

        public Startup(IHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("sqlite");
            var connectionFactory = new ConnectionFactory(connectionString);

            services.AddGrpc();
            services.AddSingleton(connectionFactory);
            services.AddScoped<ExternalApiClient, ExternalApiClient>();
            services.AddScoped<Repository, Repository>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<ChuckNorrisJokesService>();
            });
        }
    }
}
